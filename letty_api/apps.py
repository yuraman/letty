from django.apps import AppConfig


class LettyApiConfig(AppConfig):
    name = 'letty_api'
